### 2.0.3-rc.0
1.Modify serious or fatal issues detected by CodeCheck scanning

### 2.0.2
1.DevEco Studio: NEXT Beta1-5.0.3.806,SDK:API12 Release(5.0.0.66)验证通过
2.适配ArkTs语法

### 2.0.1
1.适配DevEco Studio: 4.0(4.0.3.512),SDK: API10（4.0.10.9）
2.适配ArkTs语法

### 2.0.0
1.适配DevEco Studio: 3.1 Beta2(3.1.0.400), SDK: API9 Release(3.2.11.9)。

2.包管理工具由npm切换成ohpm。

### 0.1.1

1.适配DevEco Studio 3.1 Beta1版本。

### 0.1.0

1.支持文件（夹）的创建、删除、复制、粘贴、移动、清空以及判断文件是否存在等操作，并且提供同步与异步两种方法。