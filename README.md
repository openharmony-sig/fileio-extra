# fileio-extra

## Introduction
**fileio-extra** encapsulates the **ohos.fileio** APIs and extends the **fileio** capabilities. Compared with **fileio**, **fileio-extra** provides more comprehensive file operations:

- Creating a file or folder.
- Removing a file or folder.
- Moving a file or folder (you can determine whether to overwrite the file or folder with the same name).
- Reading and writing a file.
- Clearing a folder.
- Copying a file or folder.
- Checking whether a file or folder exists.

## How to Install

```shell
ohpm install @ohos/fileio-extra
```
For details about the OpenHarmony ohpm environment configuration, see [OpenHarmony HAR](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.en.md).

## How to Use

### 1. Create a folder.

```typescript
    import fs from '@ohos/fileio-extra'
    // Create a folder synchronously.
    fs.mkdirsSync("xx/xx/dirname") // Folder path/folder name.
    // Create a folder asynchronously.
    fs.mkdirs("xx/xx/dirname").then(() => {
        console.log('Creation successful')
    }).catch(err => {
        console.log('Creation failed' + err)
    })
```

### 2. Create a text file and a JSON file.

```typescript
    import fs from '@ohos/fileio-extra'
    // Create a text file and a JSON file synchronously.
    fs.outputFileSync("xx/xx/filename.txt", 'File content') // Directory/file_name.txt.
    fs.outputJSONSync("xx/xx/filename.json", '{}', { encoding: "utf-8"}) // Directory/file_name.json.
    // Create a text file and a JSON file asynchronously.
    fs.outputFile ("xx/xx/filename.txt",'File content').then (() => {
        console.log('Creation successful')
    }).catch(err => {
        console.log('Creation failed' + err)
    })
    fs.outputJSON("xx/xx/filename.json", '{}', { encoding: "utf-8"}).then(() => {
        console.log('Creation successful')
    }).catch(err => {
        console.log('Creation failed' + err)
    })
```

### 3. Remove a file or folder.

```typescript
    import fs from '@ohos/fileio-extra'
    // Remove a file or folder synchronously.
    fs.removeSync("xx/xx/filename") // Path of the file or folder to remove.
    // Remove a file or folder asynchronously.
    fs.remove("xx/xx/filename").then(() => {
        console.log('Remove successful')
    }).catch(err => {
        console.log('Remove failed' + err)
    })
```

### 4. Copy a file or folder.

```typescript
    import fs from '@ohos/fileio-extra'
    // Copy a file or folder synchronously. The first parameter specifies the path of the file or folder to copy, and the second parameter specifies the destination path of the file or folder.
    fs.copySync("xx/folder1/filename", "xx/folder2/filename")
    // Copy a file or folder asynchronously.
    fs.copy("xx/folder1/filename", "xx/folder2/filename").then(() => {
        console.log('Copy successful')
    }).catch(err => {
        console.log('Copy failed' + err)
    })
```

### 5. Move a file or folder.

```typescript
    import fs from '@ohos/fileio-extra'
    // Move a file or folder synchronously. The first parameter specifies the path of the file or folder to move, and the second parameter specifies the destination path of the file or folder.
    fs.moveSync("xx/folder1/filename", "xx/folder2/filename")
    // Move a file or folder asynchronously.
    fs.move("xx/folder1/filename", "xx/folder2/filename").then(() => {
        console.log('Move successful')
    }).catch(err => {
        console.log('Move failed' + err)
    })
```

### 6. Check whether a file or folder exists.

```typescript
    import fs from '@ohos/fileio-extra'
    // Checks whether a file or folder exists. This API returns the result synchronously. The value true means the file or folder exists, and false means the opposite.
    let path = fs.pathExistsSync("xx/folder1/filename") // Path of the file or folder.
    console.log(path +' = true or false')
    // Checks whether a file or folder exists. This API returns the result asynchronously.
    fs.pathExists("xx/folder1/filename").then((res) => {
        console.log('true if it exists, false if it does not exist' + res)
    })
```

### 7. Clear a folder.

```typescript
    import fs from '@ohos/fileio-extra'
    // Clear a folder synchronously.
    fs.emptyDirSync("xx/folder1/filename") // Folder path (an error will be reported if the path indicates a file)
    // Clear a folder asynchronously.
    fs.emptyDir("xx/folder1/filename").then((res) => {
        console.log('Clear successful')
    })
```

## Available APIs

### fileio-extra
|     API    |                             Parameter                            |   Return Value  |                           Description                          |
| :------------: | :----------------------------------------------------------: | :--------: | :----------------------------------------------------------: |
|    copySync    | **src** (string): path of the file or folder to copy.<br> **dest** (string): destination path of the file or folder.<br> [options](#options)?:object |     No value is returned.    | Copies a file or folder. This API returns the result synchronously.<br>(If **src** is a file, **dest** must also be a file.)<br>(If **src** is a folder, **dest** must also be a folder.)|
|      copy      | **src** (string): path of the file or folder to copy.<br> **dest** (string): destination path of the file or folder.<br> [options](#options)?:object <br> cb?()=>{}: callback invoked when the copy operation is successful.|     No value is returned.    | Copies a file or folder. This API returns the result asynchronously.<br>(If **src** is a file, **dest** must also be a file.)<br>(If **src** is a folder, **dest** must also be a folder.)|
|  emptyDirSync  |                    **dir** (string): path of the folder to clear.                    |     No value is returned.    |                        Clears a folder. This API returns the result synchronously.                       |
|    emptyDir    |                    **dir** (string): path of the folder to clear.                    |     No value is returned.    |                        Clears a folder. This API returns the result asynchronously.                       |
| outputJSONSync | **file** (string): path of the JSON file to create.<br>**data** (string): content of the JSON file to create.<br> [options](#options)?:object |     No value is returned.    |                       Creates a JSON file. This API returns the result synchronously.                      |
|   outputJSON   | **file** (string): path of the JSON file to create.<br>**data** (string): content of the JSON file to create.<br> [options](#options)?:object |     No value is returned.    |                       Creates a JSON file. This API returns the result asynchronously.                      |
|   mkdirsSync   |       **dir** (string): path of the folder to create.<br> [mode](#mode)?:number       |     No value is returned.    |                        Creates a folder. This API returns the result synchronously.                       |
|     mkdirs     |       **dir** (string): path of the folder to create.<br> [mode](#mode)?:number       |     No value is returned.    |                        Creates a folder. This API returns the result asynchronously.                       |
|    moveSync    | **src** (string): path of the file or folder to move.<br> **dest** (string): destination path of the file or folder.<br> [opts](#opts)?:object |     No value is returned.    |                         Moves a file or folder. This API returns the result synchronously.                        |
|      move      | **src** (string): path of the file or folder to move.<br> **dest** (string): destination path of the file or folder.<br> [opts](#opts)?:object<br> cb?()=>{}: callback invoked when the file or folder is moved successfully. |     No value is returned.    | Moves a file or folder. This API returns the result asynchronously.<br>(If **src** is a file, **dest** must also be a file.)<br>(If **src** is a folder, **dest** must also be a folder.)|
| outputFileSync | **file** (string): path of the text file to create.<br>**data** (string): content of the text file. <br> [options](#options)?:object |     No value is returned.    |                       Creates a text file. This API returns the result synchronously.                       |
|   outputFile   | **file** (string): path of the text file to create.<br>**data** (string): content of the text file. <br> [options](#options)?:object<br>cb? ()=>{}: callback invoked when the text file is created successfully.|     No value is returned.    |                       Creates a text file. This API returns the result asynchronously.                       |
| pathExistsSync |                     **path** (string): path of the file to check.                    | true/false |                     Checks whether a file exists. This API returns the result synchronously.                    |
|   pathExists   |                     **path** (string): path of the file to check.                    | true/false |                     Checks whether a file exists. This API returns the result asynchronously.                    |
|   removeSync   |                     **path** (string): path of the file to remove.                    |     No value is returned.    |                         Removes a file. This API returns the result synchronously.                        |
|     remove     |                     **path** (string): path of the file to remove.                    |     No value is returned.    |                         Removes a file. This API returns the result asynchronously.                        |

### options
**options** supports the following parameters:
- **offset** (number): position of the data to write in reference to the start address of the data. This parameter is optional. The default value is **0**.
- **length** (number): length of the data to write. This parameter is optional. The default value is the buffer length minus the offset.
- **position** (number): start position to write the data in the file. This parameter is optional. By default, data is written from the current position.
- **encoding** (string): format of the data to be encoded when the data is a string. This parameter is optional. The default value is **'utf-8'**.
  Constraints: **offset** + **length** <= Buffer size

### mode
Permission on the folder to create. You can specify multiple permissions, separated using a bitwise OR operator (&#124;). The default value is **0o775**.
- **0o775**: The owner and other users have the read, write, and execute permissions on the folder.
- **0o700**: The owner has the read, write, and execute permissions on the folder.
- **0o400**: The owner has the read permission on the folder.
- **0o200**: The owner has the write permission on the folder.
- **0o100**: The owner has the execute permission on the folder.
- **0o070**: The user group has the read, write, and execute permissions on the folder.
- **0o040**: The user group has the read permission on the folder.
- **0o020**: The user group has the write permission on the folder.
- **0o010**: The user group has the execute permission on the folder.
- **0o007**: Other users have the read, write, and execute permissions on the folder.
- **0o004**: Other users have the read permission on the folder.
- **0o002**: Other users have the write permission on the folder.
- **0o001**: Other users have the execute permission on the folder.

### opts
**opts** supports the following parameter:
- **overwrite**: Whether to overwrite the file or folder with the same name. The value **true** means to overwrite the file or folder; the value **false** means the opposite. This parameter is optional. The default value is **false**.

## Constraints
This project has been verified in the following versions:
- DevEco Studio: NEXT Beta1-5.0.3.806, SDK: API 12 Release (5.0.0.66)
- DevEco Studio: 4.1 (4.1.3.413), SDK: API 11 (4.1.0.53)
- DevEco Studio: 4.1 (4.1.3.215), SDK: API11 (4.1.3.1)
- DevEco Studio: 4.0 (4.0.3.512), SDK: API 10 (4.0.10.9)
- DevEco Studio: 3.1 Beta2(3.1.0.400), SDK: API9 Release(3.2.11.9)

## Directory Structure
````
|----fileio-extra
|     |---- entry    # Sample code
|     |---- library  # fileio-extra code
|                |---- fileio-extra # Code for file operations
|                      |---- copy   # Code for copying a file or folder
|                      |---- empty  # Code for clearing a folder
|                      |---- ensure # Code for creating a file
|                      |---- fs     # Code that encapsulates the ohos.fileio APIs
|                      |---- json   # Code for operating JSON files
|                      |---- mkdirs # Code for creating a folder
|                      |---- move   # Code for moving a file or folder
|                      |---- output-file # Code for creating a file and writing data to it
|                      |---- path-exists # Code for checking whether a file or folder exists
|                      |---- remove # Code for removing a file or folder
|                      |---- util   # Code for obtaining file information.
|                |---- jsonfile     # Code for JSON file operations.
|                |---- node         # Implementation of some node code
|                |---- univresalify # Implementation of callbacks
|           |---- index.ets         # External APIs
|     |---- README.md            # Readme                   
````

## How to Contribute
If you find any problem during the use, submit an [Issue](https://gitee.com/openharmony-sig/fileio-extra/issues) or a [PR](https://gitee.com/openharmony-sig/fileio-extra/pulls) to us.

## License
This project is licensed under [MIT License](https://gitee.com/openharmony-sig/fileio-extra/blob/master/LICENSE).
    
