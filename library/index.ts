/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import copy from './src/main/ets/components/fileio-extra/copy'
import empty from './src/main/ets/components/fileio-extra/empty'
import ensure from './src/main/ets/components/fileio-extra/ensure'
import fs from './src/main/ets/components/fileio-extra/fs'
import json from './src/main/ets/components/fileio-extra/json'
import mkdir from './src/main/ets/components/fileio-extra/mkdirs'
import move from './src/main/ets/components/fileio-extra/move'
import outputFile from './src/main/ets/components/fileio-extra/output-file'
import remove from './src/main/ets/components/fileio-extra/remove'
import pathExists from './src/main/ets/components/fileio-extra/path-exists'

export default {
  ...fs,
  ...copy,
  ...empty,
  ...ensure,
  ...json,
  ...mkdir,
  ...move,
  ...outputFile,
  ...pathExists,
  ...remove
}