import copySync from './copy-sync'
import copy from './copy'
import { fromCallback } from '../../universalify'

export default {
    copy: fromCallback(copy),
    copySync
}