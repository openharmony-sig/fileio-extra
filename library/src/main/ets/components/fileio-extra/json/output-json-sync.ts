import { stringify } from '../../jsonfile/utils'
import outputFile from '../output-file'

export function outputJsonSync (file, data, options) {
  const str = stringify(data, options)

  outputFile.outputFileSync(file, str, options)
}