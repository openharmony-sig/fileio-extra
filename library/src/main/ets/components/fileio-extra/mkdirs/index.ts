import { fromPromise } from '../../universalify'
import { makeDir, makeDirSync } from './make-dir'

export default {
    mkdirs: fromPromise(makeDir),
    mkdirsSync: makeDirSync,
    // alias
    mkdirp: fromPromise(makeDir),
    mkdirpSync: makeDirSync,
    ensureDir: fromPromise(makeDir),
    ensureDirSync: makeDirSync
}