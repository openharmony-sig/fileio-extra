import fs from '../fs/index'
import { fromPromise } from '../../universalify/index'

function pathExists (path) {
    return new Promise((res, rej) => {
        res(fs.access(path).then(() => true).catch(() => false))
    })
}

function pathExistsSync(path) {
    return fs.existsSync(path);
}

export default {
    pathExists: fromPromise(pathExists),
    pathExistsSync
}