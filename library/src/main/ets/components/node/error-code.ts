/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

export default {
    errorcode: {
        EPERM: 1, /*Operation not permitted*/
        ENOENT: 2, /*No such file or directory*/
        ESRCH: 3, /*No such process*/
        EINTR: 4, /*interrupted system call*/
        EIO: 5, /*I/O error*/
        ENXIO: 6, /*No such device or address*/
        E2BIG: 7, /*Argument list too long*/
        ENOEXEC: 8, /*Exec format error*/
        EBADF: 9, /*Bad file number*/
        ECHILD: 10, /*No child processes*/
        EAGAIN: 11, /*Try again*/
        ENOMEM: 12, /*Out of menmory*/
        EACCES: 13, /*Permission denied*/
        EFAUIT: 14, /*Bad address*/
        ENOTBLK: 15, /*Block device required*/
        EBUSY: 16, /*Device or resource busy*/
        EEXIST: 17, /*File exists*/
        EXDEV: 18, /* Cross-device link*/
        ENOTDIR: 20, /*Not a director*/
        EISDIR: 21, /*Is a direcyory*/
        EINVAL: 22, /*Invalid argument*/
        ENFILE: 23, /*File table overflow*/
        EMFILE: 24, /*Too many open files*/
        ENOTTY: 25, /*Not a typewriter*/
        ETXBSY: 26, /*Text file busy*/
        EFBIG: 27, /*File too large*/
        ENOSPC: 28, /*No space left on device*/
        ESPIPE: 29, /*Illegal seek*/
        EROFS: 30, /*Read-only file system*/
        EMLINK: 31, /*Too many links*/
        EPIPE: 32, /*Broken pipe*/
        EDOM: 33, /*Math argument out of domain of func*/
        ERANGE: 34, /*Math result not representable*/
    },

    errormessage: {
        EPERM: 'Operation not permitted',
        ENOENT: 'No such file or directory',
        ESRCH: 'No such process',
        EINTR: 'interrupted system call',
        EIO: 'I/O error',
        ENXIO: 'No such device or address',
        E2BIG: 'Argument list too long',
        ENOEXEC: 'Exec format error',
        EBADF: 'Bad file number',
        ECHILD: 'No child processes',
        EAGAIN: 'Try again',
        ENOMEM: 'Out of menmory',
        EACCES: 'Permission denied',
        EFAUIT: 'Bad address',
        ENOTBLK: 'Block device required',
        EBUSY: 'Device or resource busy',
        EEXIST: 'File exists',
        EXDEV: ' Cross-device link',
        ENOTDIR: 'Not a director',
        EISDIR: 'Is a direcyory',
        EINVAL: 'Invalid argument',
        ENFILE: 'File table overflow',
        EMFILE: 'Too many open files',
        ENOTTY: 'Not a typewriter',
        ETXBSY: 'Text file busy',
        EFBIG: 'File too large',
        ENOSPC: 'No space left on device',
        ESPIPE: 'Illegal seek',
        EROFS: 'Read-only file system',
        EMLINK: 'Too many links',
        EPIPE: 'Broken pipe',
        EDOM: 'Math argument out of domain of func',
        ERANGE: 'Math result not representable',
        ENOTEMPTY: 'Directory not emtry'
    },

    message2code: {
        'Operation not permitted': 'EPERM',
        'No such file or directory': 'ENOENT',
        'No such process': 'ESRCH',
        'interrupted system call': 'EINTR',
        'I/O error': 'EIO',
        'No such device or address': 'ENXIO',
        'Argument list too long': 'E2BIG',
        'Exec format error': 'ENOEXEC',
        'Bad file number': 'EBADF',
        'No child processes': 'ECHILD',
        'Try again': 'EAGAIN',
        'Out of menmory': 'ENOMEM',
        'Permission denied': 'EACCES',
        'Bad address': 'EFAUIT',
        'Block device required': 'ENOTBLK',
        'Device or resource busy': 'EBUSY',
        'File exists': 'EEXIST',
        'Cross-device link': 'EXDEV',
        'Not a director': 'ENOTDIR',
        'Is a direcyory': 'EISDIR',
        'Invalid argument': 'EINVAL',
        'File table overflow': 'ENFILE',
        'Too many open files': 'EMFILE',
        'Not a typewriter': 'ENOTTY',
        'Text file busy': 'ETXBSY',
        'File too large': 'EFBIG',
        'No space left on device': 'ENOSPC',
        'Illegal seek': 'ESPIPE',
        'Read-only file system': 'EROFS',
        'Too many links': 'EMLINK',
        'Broken pipe': 'EPIPE',
        'Math argument out of domain of func': 'EDOM',
        'Math result not representable': 'ERANGE',
        'Directory not emtry': 'ENOTEMPTY'
    }
}